﻿using System;
using Microsoft.Xna.Framework;

namespace Brothelhood
{
    public struct Size
    {
        private int width, height;

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        public int Width
        {
            get { return this.width; }
            set { this.width = value; }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        public int Height
        {
            get { return this.height; }
            set { this.height = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Size"/> struct.
        /// </summary>
        /// <param name="size">The width and height.</param>
        public Size(int size)
        {
            this.width = size;
            this.height = size;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Size" /> struct.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public Size(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        #region Operators
        public static Size operator +(Size s1, Size s2)
        {
            return new Size(s1.width + s2.width, s1.height + s2.height);
        }

        public static Size operator -(Size s1, Size s2)
        {
            return new Size(s1.width - s2.width, s1.height - s2.height);
        }

        public static Size operator *(Size s1, int a)
        {
            return new Size(s1.width * a, s1.height * a);
        }

        public static Size operator *(int a, Size v1)
        {
            return v1 * a;
        }
        #endregion

        /// <summary>
        /// Gets the <see cref="Size"/> as a string.
        /// </summary>
        /// <returns><see cref="width"/> and <see cref="height"/> as a string.</returns>
        public override string ToString()
        {
            return string.Format("{0}, {1}", this.width, this.height);
        }
        
        /// <summary>
        /// Gets the center of the size.
        /// </summary>
        /// <returns>The center as a vector2.</returns>
        public Vector2 Center
        {
            get { return new Vector2(this.width * 0.5f, this.height * 0.5f); }
        }
    }
}
