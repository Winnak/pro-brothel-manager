﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Brothelhood
{
    public class Prostitute : GameObject, ICollidable
    {
        private const int WorkTime = 5;
        #region Attributes
        #region PersonalAttributes
        /// <summary>
        /// What Room the Prostitute is in
        /// </summary>
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        /// <summary>
        /// Name of the Prostitute
        /// </summary>
        private string name;
        /// <summary>
        /// The Prostitutes stagename
        /// </summary>
        private string stagename;
        /// <summary>
        /// The age of the Prostitute
        /// </summary>
        private int age;
        /// <summary>
        /// The name of the bodyType of the Prostitute
        /// </summary>
        private string bodyTypeName;
        private int bodyTypeID;
        /// <summary>
        /// The path to the bodyType of the Prostitute
        /// </summary>
        private string bodyTypePath;
        /// <summary>
        /// How experianced the Prostitute is
        /// </summary>
        private int xp;
        /// <summary>
        /// How much energy the Prostitute has
        /// </summary>
        private int energy;
        /// <summary>
        /// Determen wether the Prostitute is kidnapped or not
        /// </summary>
        private bool kidnapped;
        /// <summary>
        /// The List over the various stds
        /// </summary>
        private List<string> stds;
        /// <summary>
        /// Name of the hairstyle which this Prostitute has
        /// </summary>
        private string hairstyleName;
        private int hairstyleID;
        /// <summary>
        /// The Path to the hairstyle of the Prostitute
        /// </summary>
        private string hairstylePath;
        /// <summary>
        /// The texture for the hair of the Prostitute
        /// </summary>
        private Sprite hairstyle;
        /// <summary>
        /// Name of the color which this Prostitutes hair has
        /// </summary>
        private string hairstyleColorName;
        private int hairstyleColorID;
        private string hairstyleColorHEX;
        /// <summary>
        /// The color of the hair of the Prostitute
        /// </summary>
        private Color hairstyleColor;
        /// <summary>
        /// Name of the skincolor which this Prostitute has
        /// </summary>
        private string skincolorName;
        private int skinColorID;
        private string skinColorHEX;
        /// <summary>
        /// The texture for the skin of the Prostitute
        /// </summary>
        private Color skincolor;
        #endregion

        /// <summary>
        /// The current Customer of this Prostitute
        /// </summary>
        private Customer currentCustomer;
        /// <summary>
        /// Boolean to determin wether this Prostitute is serving a Customer
        /// </summary>
        private bool serving;
        /// <summary>
        /// Timer for how long this Prostitute will be working with a Customer
        /// </summary>
        private float workingTimer;
        /// <summary>
        /// Generate a random number... simple really
        /// </summary>
        private Random random;
        #endregion

        /// <summary>
        /// Returns the value of kidnapped when called
        /// </summary>
        public bool Kidnapped
        {
            get { return kidnapped; }
        }

        public Rectangle CollisionRect
        {
            get
            {
                return new Rectangle(
                    (int)(Position.X - this.Sprite.Origin.X),
                    (int)(Position.Y - this.Sprite.Origin.Y),
                    Sprite.SrcRectangle.Width, Sprite.SrcRectangle.Height);
            }
        }

        /// <summary>
        /// Contructer for a "new" Prostitute, used for a newly acquired Prostitutes
        /// </summary>
        public Prostitute(int id)
        {
            this.id = id;
            xp = 0;
            stds = new List<string>();
            random = new Random();
            serving = false;
            workingTimer = 0;
            SetupNewProstitute();
        }

        /// <summary>
        /// Contructer for a "old" Prostitute, used on load game
        /// </summary>
        public Prostitute(int id, string name, string stagename, int age, int xp, int energy, bool kidnapped, List<string> stds, int bodyTypeID, int hairstyleID, int hairstyleColorID, int skinColorID)
        {
            this.id = id;
            this.name = name;
            this.stagename = stagename;
            this.age = age;
            this.bodyTypeID = bodyTypeID;
            this.xp = xp;
            this.energy = energy;
            this.kidnapped = kidnapped;
            this.stds = stds;
            this.hairstyleID = hairstyleID;
            this.hairstyleColorID = hairstyleColorID;
            this.skinColorID = skinColorID;

            random = new Random();
            serving = false;
            workingTimer = 0;
            GetTextureInfo();

            Position = new Vector2(340 + ((id - 1) * 105), 500);
        }

        /// <summary>
        /// Sets up a new prostitute with random field values from DataSet
        /// </summary>
        private void SetupNewProstitute()
        {
            int randomNumber = random.Next(0, DatabaseManager.ContentDataSet.Tables["skincolor"].Rows.Count);

            skinColorHEX = DatabaseManager.ContentDataSet.Tables["skincolor"].Rows[randomNumber]["HexColor"].ToString();
            skincolor = DatabaseManager.HexToColor(skinColorHEX);
            skincolorName = DatabaseManager.ContentDataSet.Tables["skincolor"].Rows[randomNumber]["Name"].ToString();
            skinColorID = randomNumber;

            randomNumber = random.Next(0, DatabaseManager.ContentDataSet.Tables["body"].Rows.Count);

            bodyTypeName = DatabaseManager.ContentDataSet.Tables["body"].Rows[randomNumber]["Name"].ToString();
            bodyTypePath = DatabaseManager.ContentDataSet.Tables["body"].Rows[randomNumber]["FileName"].ToString();
            bodyTypeID = randomNumber;

            randomNumber = random.Next(0, DatabaseManager.ContentDataSet.Tables["haircolor"].Rows.Count);

            hairstyleColorName = DatabaseManager.ContentDataSet.Tables["haircolor"].Rows[randomNumber]["Name"].ToString();
            hairstyleColorHEX = DatabaseManager.ContentDataSet.Tables["haircolor"].Rows[randomNumber]["HexColor"].ToString();
            hairstyleColor = DatabaseManager.HexToColor(hairstyleColorHEX);
            hairstyleColorID = randomNumber;

            randomNumber = random.Next(0, DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows.Count);

            hairstylePath = DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows[randomNumber]["FileName"].ToString();
            hairstyleName = DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows[randomNumber]["Name"].ToString();
            hairstyleID = randomNumber;

            int randomNum;
            randomNum = random.Next(0, 10);

            if (randomNum <= 2)
                age = random.Next(65, 90);
            if (randomNum <= 5)
                age = random.Next(35, 64);
            if (randomNum > 5)
                age = random.Next(16, 34);

            //gets a random firstname and lastname from database between 0 and Rows.Count (number of Rows), puts them together with space between
            name = DatabaseManager.ContentDataSet.Tables["firstname"].Rows[random.Next(0, DatabaseManager.ContentDataSet.Tables["firstname"].Rows.Count)]["Name"].ToString();
            name += " ";
            name += DatabaseManager.ContentDataSet.Tables["lastname"].Rows[random.Next(0, DatabaseManager.ContentDataSet.Tables["lastname"].Rows.Count)]["Name"].ToString();

            GetSprites();

            Position = new Vector2(330 + ((id - 1) * 110), 500);
            DatabaseManager.AddProstitute(name, stagename, age, xp, energy, 0, bodyTypeID, skinColorID, hairstyleColorID, hairstyleID, 0);
        }

        /// <summary>
        /// Gets info about texture from content.db if "old" prostitute is made
        /// </summary>
        private void GetTextureInfo()
        {
            skincolorName = DatabaseManager.ContentDataSet.Tables["skincolor"].Rows[skinColorID]["Name"].ToString();
            skinColorHEX = DatabaseManager.ContentDataSet.Tables["skincolor"].Rows[skinColorID]["HexColor"].ToString();
            skincolor = DatabaseManager.HexToColor(skinColorHEX);

            // TODO BUG FIX
            bodyTypeName = DatabaseManager.ContentDataSet.Tables["body"].Rows[bodyTypeID]["Name"].ToString();
            bodyTypePath = DatabaseManager.ContentDataSet.Tables["body"].Rows[bodyTypeID]["FileName"].ToString();

            hairstyleColorName = DatabaseManager.ContentDataSet.Tables["haircolor"].Rows[hairstyleColorID]["Name"].ToString();
            hairstyleColorHEX = DatabaseManager.ContentDataSet.Tables["haircolor"].Rows[hairstyleColorID]["HexColor"].ToString();
            hairstyleColor = DatabaseManager.HexToColor(hairstyleColorHEX);

            hairstylePath = DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows[hairstyleID]["FileName"].ToString();
            hairstyleName = DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows[hairstyleID]["Name"].ToString();

            GetSprites();
        }

        private void GetSprites()
        {
            this.Sprite = new Sprite(GameManager.Content.Load<Texture2D>(bodyTypePath)); //bodytype
            this.Sprite.Color = skincolor; //bodytype color
            this.hairstyle = new Sprite(GameManager.Content.Load<Texture2D>(hairstylePath));
            this.hairstyle.Color = hairstyleColor;
            this.hairstyle.Origin = new Vector2(0, 8);
        }

        public override void Update(GameTime time)
        {
            if (serving)
            {
                workingTimer -= (float)time.ElapsedGameTime.TotalSeconds;

                if (workingTimer <= 0)
                {
                    serving = false;
                    currentCustomer.currentState = Customer.State.Leaving;
                    Brothel.Instance.cash += currentCustomer.CollectPayment();
                    Brothel.Instance.MonthlyIncome += currentCustomer.CollectPayment();
                    currentCustomer = null;
                }

                return;
            }

            MouseState state = Mouse.GetState();

            if (state.LeftButton == ButtonState.Pressed)
            {
                Point mousePoint = new Point(state.X, state.Y);

                if (this.CollisionRect.Contains(mousePoint))
                {
                    if (GameManager.SelectedCustomer != null)
                    {
                        SetCustomer(GameManager.SelectedCustomer);
                        this.serving = true;
                        GameManager.SelectedCustomer = null;
                        return;
                    }

                    GUIInfoElement.Instance.Remove();
                    GUIInfoElement.Instance.screenText =
                        stagename == string.Empty ? " '" + stagename + "' " : " " + name + ", Age " + age.ToString();
                    GUIInfoElement.Instance.buttonOneText = "Send away";
                    GUIInfoElement.Instance.ButtonOne = RemoveProstitute();
                }
            }
        }

        public override void Draw(GameTime time, SpriteBatch sb)
        {
            sb.Draw(this.Sprite.Texture, Position, this.Sprite.SrcRectangle, this.Sprite.Color, 0, Sprite.Origin, 1, Sprite.Effect, -0.1f);
            sb.Draw(this.hairstyle.Texture, Position, this.hairstyle.SrcRectangle, this.hairstyle.Color, 0, this.hairstyle.Origin, 1, this.hairstyle.Effect, -0.05f);
#if DEBUG
            sb.Draw(Asset.Image["Pixel"], CollisionRect, new Rectangle(0, 0, 1, 1), new Color(Color.Aquamarine, 128), 0, Vector2.Zero, SpriteEffects.None, 1);
#endif
        }

        private Action RemoveProstitute()
        {
            return this.Dispose;
        }

        public void SetCustomer(Customer customer)
        {
            //For det første vindue vil dette nok være new Vector(313, 498).
            workingTimer = WorkTime + random.Next(5);
            ScreenEffects.WindowClosing(new Vector2(313 + ((id - 1) * 110), 498), workingTimer);
            currentCustomer = customer;
            currentCustomer.currentState = Customer.State.Active;
            //Give the customer info about his girl, to determin the rating
            currentCustomer.GetProstituteInfo(new List<string> { hairstyleName, hairstyleColorName, skincolorName, bodyTypeName });
        }

        public override void Dispose()
        {
            DatabaseManager.RemoveProstitute(id);
            base.Dispose();
        }
    }
}
