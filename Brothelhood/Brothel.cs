﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Brothelhood
{
    public class Brothel : GameObject, ICollidable
    {
        public int cash;
        public int floors;
        private int lastMafiaPayment;
        private int mafiaPayment;

        private int upgradecost = 1000;
        public int rating;
        public int notoriety;
        private int MaxNotoriety;
        private double totalSeconds;
        private double monthTimer;
        private double weeklyTimer;
        private int numberOfRatings;
        private int totalRating;
        private int monthlyIncome;
        private float mafiaPCT;
        private Texture2D window;

        public int MonthlyIncome
        {
            get { return this.monthlyIncome; }
            set { this.monthlyIncome = value; }
        }

        private static Brothel instance;

        public static Brothel Instance
        {
            get 
            { 
                if (instance == null)
                {
                    instance = new Brothel();
                }
                return instance;
            }
        }

        public Rectangle CollisionRect
        {
            get { 
                return new Rectangle(
                    (int)Position.X, (int)Position.Y, 
                    Sprite.SrcRectangle.Width, Sprite.SrcRectangle.Height); 
            }
        }
        public string Rating
        {
            get {
                string rating = string.Empty;

                for (int s = 0; s < this.rating; s++)
                {
                    rating += "*";
                }

                return rating;
            }
     
        }
        public string Rent
        {
            get { return mafiaPayment.ToString(); }
        }
        
        public Brothel()
        {
            this.Sprite = new Sprite(GameManager.Content.Load<Texture2D>(@"brothel"));
            this.Sprite.Origin = new Vector2(this.Sprite.Texture.Width * 0.5f, 0);
            mafiaPCT = 0.5f;
            monthlyIncome = 0;
            MaxNotoriety = 100; // or something!
            Sprite.SrcRectangle = new Rectangle(0, 0, Sprite.Texture.Width, Sprite.Texture.Height);

            this.window = Asset.Image["Pixel"];

            SetBrothel();
            AddPreviousProstitutes();
        }

        private void SetBrothel()
        {
            totalSeconds = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["brothel"].Rows[0]["TotalSeconds"]);
            cash = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["brothel"].Rows[0]["Cash"]);
            floors = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["brothel"].Rows[0]["Floors"]);
            mafiaPayment = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["brothel"].Rows[0]["MafiaPayment"]);
            rating = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["brothel"].Rows[0]["Rating"]);
            notoriety = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["brothel"].Rows[0]["Notoriety"]);
            
            SetFloors(floors);
        }

        /// <summary>
        /// Adds previous prostitutes from DataSet
        /// </summary>
        private void AddPreviousProstitutes()
        {
            int numberOfProstitutes = DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows.Count;

            for (int i = 0; i < numberOfProstitutes; i++)
            {
                int id = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["ID"]);
                string name = DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["Name"].ToString();
                string stageName = DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["StageName"].ToString();
                int age = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["Age"]);
                int xp = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["Xp"]);
                int energy = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["Energy"]);
                bool kidnapped = Convert.ToBoolean(DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["Kidnapped"]);
                int bodyID = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["BodyID"]);
                int skinColorID = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["SkinColorID"]);
                int hairColorID = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["HairColorID"]); 
                int hairStyleID = Convert.ToInt32(DatabaseManager.BrothelDataSet.Tables["prostitute"].Rows[i]["HairStyleID"]);

                Prostitute prost = new Prostitute(id, name, stageName, age, xp, energy, kidnapped, new List<string>(), bodyID, hairStyleID, hairColorID, skinColorID);
            }
        }

        public override void Update(GameTime time)
        {
            this.totalSeconds += time.ElapsedGameTime.TotalSeconds;
            monthTimer += time.ElapsedGameTime.TotalSeconds;
            weeklyTimer += time.ElapsedGameTime.TotalSeconds;

            if (monthTimer >= 300)
            {
                MonthlyActions();
            }

            if (weeklyTimer >= 75)
            {
                WeeklyActions();
            }
        }

        public override void Draw(GameTime time, SpriteBatch sb)
        {
            sb.Draw(Sprite.Texture, this.Position, Sprite.SrcRectangle, Sprite.Color, 0, Sprite.Origin, 1, Sprite.Effect, 0);
            Rectangle windowRec = new Rectangle(313, (int)this.Position.Y + 167, 398, floors * 100);
            sb.Draw(window, windowRec, new Rectangle(0, 0, 1, 1), new Color(244, 230, 147), 0, Vector2.Zero, SpriteEffects.None, -0.5f);
        }

        private void Payment()
        {
            cash -= mafiaPayment;
        }

        private void MafiaPaymentIncrease()
        {
                        
            mafiaPayment = (int)((float)monthlyIncome * mafiaPCT);
           
            if (mafiaPayment <= lastMafiaPayment)
            {
                mafiaPayment = lastMafiaPayment;
            }
            else
            {
                new GUIWindow(
                    "The Mafia now wants you to pay\nthem more money!\n You now have to pay "+mafiaPayment, 
                    Pay(), "Pay");
            }
            lastMafiaPayment = mafiaPayment;

        }

        private void SetFloors(int floors)
        {
            this.floors = floors;
            this.Position = new Vector2(GameManager.GameSize.Width * 0.5f, (-159 - 178 - (floors * 100) + GameManager.GameSize.Height));
        }

        private Action Pay()
        {
            return this.Nothing;
        }

        private void Nothing()
        { }
        
        public void UpgradeBrothel()
        {
            //upgrade commands
            cash -= upgradecost;
            floors++;
            SetFloors(floors);
            
        }

        public void Rate(int CustomerRating)
        {
            // TODO: Bi-monthly rating reset?
            numberOfRatings++;
            totalRating += CustomerRating;
            rating = totalRating / numberOfRatings;
        }

        public void Notoriety(int notorietyIncrease)
        {
            notoriety += notorietyIncrease;
        }

        private void NotorietyCheck()
        {
            if (notoriety >= MaxNotoriety)
            {

                new GUIWindow("Your notoriety is high. There is a chance the police might invastigate",OKButtom(),"OK");

                if (GameManager.policeCount == 0)
                {

                    Random random = new Random();
                    int Policechance = random.Next(1, 101);

                    if (Policechance < 50)
                    {
                        //no policevisit
                    }
                    if (Policechance >= 50)
                    {
                        GameManager.policeCount = 3;
                        //new Police();
                    }
                }
            }   
        }

        private Action OKButtom()
        {
            return null;
        }

        private void PoliceVisit()
        {
            if (GameManager.policeCount > 0)
            {
                new Police();
            }
        }

        private void MonthlyActions()
        {
            NotorietyCheck();
            MafiaPaymentIncrease();
            Payment();
            monthTimer = 0;
            monthlyIncome = 0;
        }

        private void WeeklyActions()
        {
            PoliceVisit();

            weeklyTimer = 0;
        }

    }
}