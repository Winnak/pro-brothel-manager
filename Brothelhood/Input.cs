﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Asteroids
{
    public static class Input
    {
        private static List<Keys> inputKeyList;
        private static Dictionary<Keys, bool> inputLastFrame;
        private static Dictionary<Keys, bool> inputThisFrame;

        private static void InitializeInput()
        {
            inputKeyList = new List<Keys>();
            inputLastFrame = new Dictionary<Keys, bool>();
            inputThisFrame = new Dictionary<Keys, bool>();

            foreach (Keys value in typeof(Keys).GetEnumValues())
            {
                if (!inputKeyList.Contains(value))
                {
                    inputKeyList.Add(value);
                    inputLastFrame.Add(value, false);
                    inputThisFrame.Add(value, false);
                }
            }
        }

        public static void Update()
        {
            if (inputKeyList == null || inputLastFrame == null || inputThisFrame == null)
            {
                InitializeInput();
            }

            foreach (Keys key in inputKeyList)
            {
                inputLastFrame[key] = inputThisFrame[key];
                inputThisFrame[key] = Keyboard.GetState().IsKeyDown(key);
            }
        }

        public static bool GetKey(Keys key)
        {
            return inputThisFrame[key];
        }

        public static bool GetKeyDown(Keys key)
        {
            return inputThisFrame[key] && !inputLastFrame[key];
        }

        public static bool GetKeyUp(Keys key)
        {
            return !inputThisFrame[key] && inputLastFrame[key];
        }

        public static Vector2 GetAxis(bool normalized = false)
        {
            int x = 0, y = 0;

            if (GetKey(Keys.A))
            {
                x -= 1;
            }

            if (GetKey(Keys.D))
            {
                x += 1;
            }

            if (GetKey(Keys.W))
            {
                y -= 1;
            }
            if (GetKey(Keys.S))
            {
                y += 1;
            }

            return new Vector2(x, y);
        }
    }
}
