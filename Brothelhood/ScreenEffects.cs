﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Brothelhood
{
    /// <summary>
    /// The necessities for an effect object.
    /// </summary>
    interface IEffect
    {

    }

    /// <summary>
    /// Class for screen effects, such as interlaces, screen shakes and so on.
    /// </summary>
    public static class ScreenEffects
    {
        public static void WindowClosing(Vector2 position, double time)
        {
            new WindowEffect(position, time);
        }

        /// <summary>
        /// Effect that opens and closes a window
        /// </summary>
        private class WindowEffect : GameObject, IEffect
        {
            private Rectangle rect;
            private double timer;
            private int height;
            private const double Time = 1;
            private double workTime;

            #region Constructors
            public WindowEffect(Vector2 position, double time)
            {
                this.height = 0;
                this.workTime = time;
                this.timer = Time;
                this.rect = new Rectangle((int)position.X, (int)position.Y, 68, 52);
                this.Sprite = new Sprite(Asset.Image["Pixel"]);
                this.Sprite.Color = new Color(16, 16, 16);
            }

            public WindowEffect(Vector2 position, Size size, double time)
            {
                this.height = 0;
                this.workTime = time;
                this.timer = Time;
                this.rect =
                    new Rectangle((int)position.X, (int)position.Y,
                                       size.Width, size.Height);
                this.Sprite = new Sprite(Asset.Image["Pixel"]);
                this.Sprite.Color = new Color(16, 16, 16);
            }

            public WindowEffect(Rectangle rect, double time)
            {
                this.height = 0;
                this.workTime = time;
                this.timer = Time;
                this.rect = rect;
                this.Sprite = new Sprite(Asset.Image["Pixel"]);
                this.Sprite.Color = new Color(16, 16, 16);
            }
            #endregion

            public override void Update(GameTime time)
            {
                if (height < 52) // Lazy way to do it, TODO use max size.
                {
                    height += 3;
                    rect.Height = height;
                }

                workTime -= time.ElapsedGameTime.TotalSeconds;

                if (workTime < 0)
                {
                    timer -= time.ElapsedGameTime.TotalSeconds * 4;

                    if (timer < 0)
                    {
                        this.Dispose();
                        return; // Burde ikke være nødvændigt, men for en sikkerhedsskyld
                    }

                    rect.Height = (int)((double)height * (timer / Time));
                }

            }

            public override void Draw(GameTime time, SpriteBatch sb)
            {
                sb.Draw(this.Sprite.Texture, this.rect, new Rectangle(0, 0, 1, 1), this.Sprite.Color, 0, Vector2.Zero, SpriteEffects.None, 0.1f);
            }
        }
    }
}
