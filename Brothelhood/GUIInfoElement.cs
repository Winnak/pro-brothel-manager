﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Brothelhood
{
    class GUIInfoElement : GameObject
    {
        private Rectangle window;
        public string screenText;

        public string buttonOneText;
        public Action ButtonOne;
        public string buttonTwoText;
        public Action ButtonTwo;
        private Rectangle buttonOneRect;
        private Rectangle buttonTwoRect;

        private SpriteFont font = Asset.Font["TextSmall"];
        private Texture2D windowBackround = Asset.Image["Pixel"];

        public GUIInfoElement()
        {
            this.screenText = string.Empty;
            this.buttonOneText = string.Empty;
            this.buttonTwoText = string.Empty;
            this.window = new Rectangle(625, 600, 400, 150);
            this.buttonOneRect = new Rectangle(300, 600, 300, 64);
            this.buttonTwoRect = new Rectangle(300, 670, 300, 64);
        }

        private static GUIInfoElement instance;

        public static GUIInfoElement Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GUIInfoElement();
                }

                return instance;
            }
        }

        public void Remove()
        {
            screenText = string.Empty;
            ButtonOne = null;
            buttonOneText = string.Empty;
            ButtonOne = null;
            buttonTwoText = string.Empty;
        }

        public override void Draw(GameTime time, SpriteBatch sb)
        {
            if (screenText != string.Empty)
            {
                sb.Draw(windowBackround, window, new Rectangle(0, 0, 1, 1), Color.DarkGray, 0, Vector2.Zero, SpriteEffects.None, 0.9f);
                sb.DrawString(font, screenText, new Vector2(window.X, window.Y), Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1);

                if (buttonOneText != string.Empty)
                {
                    sb.DrawString(font, buttonOneText, new Vector2(305, 605), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 1);
                    sb.Draw(windowBackround, buttonOneRect, new Rectangle(0, 0, 1, 1), Color.DarkGray, 0, Vector2.Zero, SpriteEffects.None, 0.9f);
                }

                if (buttonTwoText != string.Empty)
                {
                    sb.DrawString(font, buttonTwoText, new Vector2(305, 675), Color.White, 0, Vector2.Zero, 1, SpriteEffects.None, 1);
                    sb.Draw(windowBackround, buttonTwoRect, new Rectangle(0, 0, 1, 1), Color.DarkGray, 0, Vector2.Zero, SpriteEffects.None, 0.9f);
                }
            }
        }

        public override void Update(GameTime time)
        {
            MouseState state = Mouse.GetState();

            if (state.LeftButton == ButtonState.Pressed)
            {
                Point mousePoint = new Point(state.X, state.Y);

                if (this.ButtonOne != null)
                {
                    if (this.buttonOneRect.Contains(mousePoint))
                    {
                        this.ButtonOne();
                        this.Remove();
                        return;
                    }
                }

                if (this.ButtonTwo != null)
                {
                    if (this.buttonTwoRect.Contains(mousePoint))
                    {
                        this.ButtonTwo();
                        this.Remove();
                        return;
                    }
                }
            }
        }
    }
}
