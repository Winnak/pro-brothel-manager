﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SQLite;
using System.Data.SQLite.Linq;
using System.Data.Entity;
using System.IO;
using Microsoft.Xna.Framework;

namespace Brothelhood
{
    public static class DatabaseManager
    {

        private const string BrothelPath = @"brothel.db";
        private const string ContentPath = @"Content\content.db";

        private static DataSet brothelDataSet = new DataSet();
        public static DataSet BrothelDataSet
        {
            get { return brothelDataSet; }
            set { brothelDataSet = value; }
        }

        private static DataSet contentDataSet = new DataSet();
        public static DataSet ContentDataSet
        {
            get { return contentDataSet; }
            set { contentDataSet = value; }
        }

        private static SQLiteConnection brothel = new SQLiteConnection(@"Data Source=" + BrothelPath + ";Version=3;New=False;Compress=True;");
        private static SQLiteConnection content = new SQLiteConnection(@"Data Source=" + ContentPath + ";Version=3;New=False;Compress=True;");

        /// <summary>
        /// Loads content.db and fills contentDataSet
        /// </summary>
        public static void LoadContent()
        {
            if (!File.Exists(ContentPath))
                return;

            Connect(content);

            FillContentDataSet(contentDataSet, content, "body");
            FillContentDataSet(contentDataSet, content, "haircolor");
            FillContentDataSet(contentDataSet, content, "hairstyle");
            FillContentDataSet(contentDataSet, content, "skincolor");
            FillContentDataSet(contentDataSet, content, "std");
            FillContentDataSet(contentDataSet, content, "firstname");
            FillContentDataSet(contentDataSet, content, "lastname");
        }

        /// <summary>
        /// Loads brothel.db (creates if necessary) and fills brothelDataSet
        /// </summary>
        public static void LoadBrothel()
        {
            if (!File.Exists(BrothelPath))
                CreateBrothel();
            
            Connect(brothel);

            FillContentDataSet(brothelDataSet, brothel, "brothel");
            FillContentDataSet(brothelDataSet, brothel, "infection");
            FillContentDataSet(brothelDataSet, brothel, "prostitute");
        }

        /// <summary>
        /// Fills given DataSet with data from given string table name via given SQLiteConnection
        /// </summary>
        private static void FillContentDataSet(DataSet ds, SQLiteConnection con, string tblName)
        {
            using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM " + tblName, con))
            {
                DataTable dt = new DataTable(tblName);
                da.Fill(dt);

                ds.Tables.Add(dt);
            }
        }

        /// <summary>
        /// Creates brothel.db and populates it with default values
        /// </summary>
        private static void CreateBrothel()
        {
            SQLiteConnection.CreateFile(BrothelPath);

            Connect(brothel);

            // Create brothel table
            string cmd;
            cmd = "CREATE TABLE brothel(";
            cmd += "ID INTEGER PRIMARY KEY AUTOINCREMENT, ";
            cmd += "TotalSeconds INTEGER NOT NULL, ";
            cmd += "Cash INTEGER NOT NULL, ";
            cmd += "Floors INTEGER NOT NULL, ";
            cmd += "MafiaPayment INTEGER NOT NULL, ";
            cmd += "Rating INTEGER NOT NULL, ";
            cmd += "Notoriety INTEGER NOT NULL);";
            ExecuteQuery(brothel, cmd);

            // Insert default values into brothel
            cmd = "INSERT INTO brothel (TotalSeconds, Cash, Floors, MafiaPayment, Rating, Notoriety) ";
            cmd += "VALUES (0, 0, 1, 0, 0, 0);";
            ExecuteQuery(brothel, cmd);

            // Create prostitute table
            cmd = "CREATE TABLE prostitute(";
            cmd += "ID INTEGER PRIMARY KEY AUTOINCREMENT, ";
            cmd += "Name VARCHAR(64) NOT NULL, ";
            cmd += "StageName VARCHAR(64) NOT NULL, ";
            cmd += "Age INTEGER NOT NULL, ";
            cmd += "Xp INTEGER NOT NULL, ";
            cmd += "Energy INTEGER NOT NULL, ";
            cmd += "Kidnapped INTEGER NOT NULL, ";
            cmd += "BodyID INTEGER NOT NULL, ";
            cmd += "SkinColorID INTEGER NOT NULL, ";
            cmd += "HairColorID INTEGER NOT NULL, ";
            cmd += "HairStyleID INTEGER NOT NULL, ";
            cmd += "RoomID INTEGER NOT NULL);";
            ExecuteQuery(brothel, cmd);

            // Insert default prostitutes
            cmd = "INSERT INTO prostitute (Name, StageName, Age, Xp, Energy, Kidnapped, BodyID, SkinColorID, HairColorID, HairStyleID, RoomID) VALUES (";
            cmd += "'Poliana Lorhina', ";
            cmd += "'Wendy', ";
            cmd += 25 + ", ";
            cmd += 0 + ", ";
            cmd += 100 + ", ";
            cmd += 0 + ", ";
            cmd += 1 + ", ";
            cmd += 1 + ", ";
            cmd += 1 + ", ";
            cmd += 1 + ", ";
            cmd += 1 + ");";
            ExecuteQuery(brothel, cmd);

            cmd = "INSERT INTO prostitute (Name, StageName, Age, Xp, Energy, Kidnapped, BodyID, SkinColorID, HairColorID, HairStyleID, RoomID) VALUES (";
            cmd += "'Sol Baianha', ";
            cmd += "'Crystal', ";
            cmd += 25 + ", ";
            cmd += 0 + ", ";
            cmd += 100 + ", ";
            cmd += 0 + ", ";
            cmd += 2 + ", ";
            cmd += 2 + ", ";
            cmd += 2 + ", ";
            cmd += 2 + ", ";
            cmd += 2 + ");";
            ExecuteQuery(brothel, cmd);

            // Create infection table
            cmd = "CREATE TABLE infection(";
            cmd += "ProstituteID INTEGER NOT NULL, ";
            cmd += "STDID INTEGER NOT NULL);";
            ExecuteQuery(brothel, cmd);
        }

        /// <summary>
        /// Saves brothelDataSet to brothel.db
        /// </summary>
        public static void SaveBrothel()
        {
            Connect(brothel);

            // Store brothel
            string cmd;
            DataTable dtBrothel = brothelDataSet.Tables["brothel"];
                
            cmd = "UPDATE brothel SET ";
            cmd += "TotalSeconds=" + dtBrothel.Rows[0]["TotalSeconds"] + ", ";
            cmd += "Cash=" + dtBrothel.Rows[0]["Cash"] + ", ";
            cmd += "Floors=" + dtBrothel.Rows[0]["Floors"] + ", ";
            cmd += "MafiaPayment=" + dtBrothel.Rows[0]["MafiaPayment"] + ", ";
            cmd += "Rating=" + dtBrothel.Rows[0]["Rating"] + ", ";
            cmd += "Notoriety=" + dtBrothel.Rows[0]["Notoriety"];
            ExecuteQuery(brothel, cmd);

            // Store infection
            cmd = "DELETE FROM infection";
            ExecuteQuery(brothel, cmd);

            DataTable dtInfection = brothelDataSet.Tables["infection"];
            for (int i = 0; i < dtInfection.Rows.Count; i++)
			{
                cmd = "INSERT INTO infection VALUES (";
                cmd += dtInfection.Rows[i]["ProstituteID"] + ", ";
                cmd += dtInfection.Rows[i]["STDID"] + ");";
                ExecuteQuery(brothel, cmd);
			}

            // Store prostitute
            cmd = "DELETE FROM prostitute";
            ExecuteQuery(brothel, cmd);

            DataTable dtProstitute = brothelDataSet.Tables["prostitute"];
            for (int i = 0; i < dtProstitute.Rows.Count; i++)
            {
                if (Convert.ToInt32(dtProstitute.Rows[i]["ID"]) == 0)
                {
                    cmd = "INSERT INTO prostitute (Name, StageName, Age, Xp, Energy, Kidnapped, BodyID, SkinColorID, HairColorID, HairStyleID, RoomID) VALUES (";
                }
                else
                {
                    cmd = "INSERT INTO prostitute VALUES (";
                    cmd += dtProstitute.Rows[i]["ID"] + ", ";
                }

                cmd += "'" + dtProstitute.Rows[i]["Name"] + "', ";
                cmd += "'" + dtProstitute.Rows[i]["StageName"] + "', ";
                cmd += dtProstitute.Rows[i]["Age"] + ", ";
                cmd += dtProstitute.Rows[i]["Xp"] + ", ";
                cmd += dtProstitute.Rows[i]["Energy"] + ", ";
                cmd += dtProstitute.Rows[i]["Kidnapped"] + ", ";
                cmd += dtProstitute.Rows[i]["BodyID"] + ", ";
                cmd += dtProstitute.Rows[i]["SkinColorID"] + ", ";
                cmd += dtProstitute.Rows[i]["HairColorID"] + ", ";
                cmd += dtProstitute.Rows[i]["HairStyleID"] + ", ";
                cmd += dtProstitute.Rows[i]["RoomID"] + ");";
                ExecuteQuery(brothel, cmd);
            }

            Disconnect(brothel);
        }

        /// <summary>
        /// Executes a given string of SQL commands on a given SQLiteConnection
        /// </summary>
        static void ExecuteQuery(SQLiteConnection con, string txtQuery)
        {
            using (SQLiteCommand sqlCmd = con.CreateCommand())
            {
                sqlCmd.CommandText = txtQuery;
                sqlCmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Adds new infection DataRow with given values into brothelDataSet
        /// </summary>
        public static void AddInfection(int prostituteID, int stdID)
        {
            DataRow[] foundRows;

            // Check if prostituteID actually exists in brothelDataSet
            foundRows = brothelDataSet.Tables["prostitute"].Select("ID = '" + prostituteID + "'");
            if (foundRows.Count() <= 0)
            {
                Console.WriteLine("ERROR: Prostitute doesn't exist in brothelDataSet. ProstituteID = " + prostituteID);
                return;
            }

            // Check if stdID actually exists in contentDataSet
            foundRows = contentDataSet.Tables["std"].Select("ID = '" + stdID + "'");
            if (foundRows.Count() <= 0)
            {
                Console.WriteLine("ERROR: STD doesn't exist in contentDataSet. STDID = " + stdID);
                return;
            }

            // Check if infection already exists in brothelDataSet
            foundRows = brothelDataSet.Tables["infection"].Select("ProstituteID = '" + prostituteID + "' AND STDID = '" + stdID + "'");

            if (foundRows.Count() > 0)
            {
                Console.WriteLine("ERROR: Infection already exists. ProstituteID = " + prostituteID + " STDID = " + stdID);
                return;
            }

            // Add infection to brothelDataSet
            DataRow newInfectionRow = brothelDataSet.Tables["infection"].NewRow();

            newInfectionRow["ProstituteID"] = prostituteID;
            newInfectionRow["STDID"] = stdID;

            brothelDataSet.Tables["infection"].Rows.Add(newInfectionRow);
        }

        /// <summary>
        /// Removes infection DataRows that match prostituteID and stdID
        /// </summary>
        public static void RemoveInfection(int prostituteID, int stdID)
        {
            DataTable dt = brothelDataSet.Tables["infection"];

            DataRow[] foundRows;
            foundRows = dt.Select("ProstituteID = " + prostituteID + " AND STDID = " + stdID);

            if (foundRows.Count() <= 0)
                Console.WriteLine("ERROR: Infection does not exists. ProstituteID = " + prostituteID + " STDID = " + stdID);

            foreach (DataRow row in foundRows)
            {
                dt.Rows.Remove(row);
            }
        }

        /// <summary>
        /// Adds new prostitue DataRow with given values into brothelDataSet
        /// </summary>
        public static void AddProstitute(string name, string stageName, int age, int xp, int energy, int kidnapped, int bodyID, int skinColorID, int hairColorID, int hairStyleID, int roomID)
        {
            DataRow newProstituteRow = brothelDataSet.Tables["prostitute"].NewRow();

            newProstituteRow["ID"] = 0;
            newProstituteRow["Name"] = name;
            newProstituteRow["StageName"] = stageName;
            newProstituteRow["Age"] = age;
            newProstituteRow["Xp"] = xp;
            newProstituteRow["Energy"] = energy;
            newProstituteRow["Kidnapped"] = kidnapped;
            newProstituteRow["BodyID"] = bodyID;
            newProstituteRow["SkinColorID"] = skinColorID;
            newProstituteRow["HairColorID"] = hairColorID;
            newProstituteRow["HairStyleID"] = hairStyleID;
            newProstituteRow["RoomID"] = roomID;

            brothelDataSet.Tables["prostitute"].Rows.Add(newProstituteRow);
        }

        /// <summary>
        /// Removes prostitute DataRow that matches prostituteID and removes all infections with matching ID
        /// </summary>
        public static void RemoveProstitute(int prostituteID)
        {
            // Remove prostitute
            DataTable dt = brothelDataSet.Tables["prostitute"];

            DataRow[] foundRows;
            foundRows = dt.Select("ID = " + prostituteID);

            if (foundRows.Count() <= 0)
            {
                Console.WriteLine("ERROR: Prostitute does not exists. ProstituteID = " + prostituteID);
                return;
            }

            foreach (DataRow row in foundRows)
            {
                dt.Rows.Remove(row);
            }
            
            // Remove infections
            dt = brothelDataSet.Tables["infection"];
            foundRows = dt.Select("ProstituteID = " + prostituteID);

            foreach (DataRow row in foundRows)
            {
                dt.Rows.Remove(row);
            }
        }

        /* Depricated "Erik's stuff"
        public static void ConnectBrothelDatabase()
        {
            string command;
            SQLiteDataAdapter adapter;
            
            if (!File.Exists(BrothelPath))
            {
                SQLiteConnection.CreateFile(BrothelPath);

                brothel = new SQLiteConnection(string.Format(
                    @"Data Source={0};Version=3;New=False};Compress=True;", BrothelPath));

                Connect(brothel);

                command = "CREATE TABLE Brothel(";
                command += "TotalSeconds int, ";
                command += "Cash int, ";
                command += "Floors int, ";
                command += "MafiaPayment int, ";
                command += "Rating int, ";
                command += "Notoriety int);";

                adapter = new SQLiteDataAdapter(command, brothel);

                brothelDataSet.Reset();
                adapter.Fill(brothelDataSet);

                Disconnect(brothel);
                return;
            }

            brothel = new SQLiteConnection(string.Format(
                    @"Data Source={0};Version={1};New={2};Compress={3};",
                         BrothelPath,         3,   false,      true));

            command = "CREATE TABLE IF NOT EXISTS Brothel(";
            command += "TotalSeconds int, ";
            command += "Cash int, ";
            command += "Floors int, ";
            command += "MafiaPayment int, ";
            command += "Rating int, ";
            command += "Notoriety int);";

            adapter = new SQLiteDataAdapter(command, brothel);

            brothelDataSet.Reset();
            adapter.Fill(brothelDataSet);

            return;
        }
        */

        /// <summary>
        /// Opens a connection gently.
        /// </summary>
        private static void Connect(SQLiteConnection database)
        {
            if (database.State == ConnectionState.Closed)
            {
                database.Open();
            }
        }

        /// <summary>
        /// Closes the connection gently.
        /// </summary>
        private static void Disconnect(SQLiteConnection database)
        {
            if (database.State == ConnectionState.Open)
            {
                database.Close();
            }
        }

        /// <summary>
        /// Disconnects all connections.
        /// </summary>
        public static void DisconnectAll()
        {
            if (brothel.State == ConnectionState.Open)
            {
                brothel.Close();
            }

            if (content.State == ConnectionState.Open)
            {
                content.Close();
            }
        }

        /// <summary>
        /// Converts a string of hexadecimals into a Color
        /// </summary>
        public static Color HexToColor(string hex)
        {
            int r = Int32.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            int g = Int32.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            int b = Int32.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);

            return new Color(r, g, b);
        }
    }
}
