﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Brothelhood
{
    public class Customer : GameObject, ICollidable
    {
        #region Attributes
        #region PersonalAttributes
        /// <summary>
        /// Name of the Customer
        /// </summary>
        private string name;
        /// <summary>
        /// The age of the Customer
        /// </summary>
        private int age;
        /// <summary>
        /// The name of the Bodytype of the Customer
        /// </summary>
        private string bodyTypeName;
        /// <summary>
        /// The path to the bodyType of the Customer
        /// </summary>
        private string bodyTypePath;
        /// <summary>
        /// Name of the hairstyle which this Customer has
        /// </summary>
        private string hairstyleName;
        /// <summary>
        /// The Path to the hairstyle of the Customer
        /// </summary>
        private string hairstylePath;
        /// <summary>
        /// The texture for the hair of the Customer
        /// </summary>
        //private Sprite hairstyle;
        /// <summary>
        /// Name of the color which this Customer hair has
        /// </summary>
        private string hairstyleColorName;
        private string hairstyleColorID;
        /// <summary>
        /// The color of the hair of the Customer
        /// </summary>
        private Color hairstyleColor;
        /// <summary>
        /// Name of the skincolor which this Customer has
        /// </summary>
        private string skincolorName;
        private string skindcolorID;
        /// <summary>
        /// The texture for the skin of the Customer
        /// </summary>
        private Color skincolor;
        #endregion

        /// <summary>
        /// The base amount of money that the customer will pay if served
        /// </summary>
        private int payment;
        /// <summary>
        /// List over this custemors interests
        /// </summary>
        private List<String> fetishes;
        /// <summary>
        /// Base rating which the Customer gives
        /// </summary>
        private float rating;
        /// <summary>
        /// Info about the Prostitute which this Customer have been assigned to if any
        /// </summary>
        private List<string> prostituteInfo;
        /// <summary>
        /// Generate a random number... simple really
        /// </summary>
        private Random random;
        #endregion

        public enum State { Entering, Waiting, Active, Leaving }
        public State currentState;

        public Rectangle CollisionRect
        {
            get
            {
                return new Rectangle(
                    (int)Position.X, (int)Position.Y,
                    Sprite.SrcRectangle.Width, Sprite.SrcRectangle.Height);
            }
        }

        public string Name
        {
            get { return name; }
        }
        public string Age
        {
            get { return Convert.ToString(age); }
        }
        public List<string> Fetishes
        {
            get { return fetishes; }
        }

        /// <summary>
        /// Contructer for a new Customer
        /// </summary>
        public Customer()
        {
            currentState = State.Entering;
            payment = 500;
            rating = 1;
            random = new Random();
            SetCustomer();
            GetSprites();
            Position = new Vector2(0, 515);
        }

        /// <summary>
        /// Define this Customer with values if called
        /// </summary>
        private void SetCustomer()
        {
            int randomNumber = random.Next(1, DatabaseManager.ContentDataSet.Tables["skincolor"].Rows.Count);

            skindcolorID = DatabaseManager.ContentDataSet.Tables["skincolor"].Rows[randomNumber]["HexColor"].ToString();
            skincolor = DatabaseManager.HexToColor(skindcolorID);
            skincolorName = DatabaseManager.ContentDataSet.Tables["skincolor"].Rows[randomNumber]["Name"].ToString();

            randomNumber = random.Next(1, DatabaseManager.ContentDataSet.Tables["body"].Rows.Count);

            bodyTypeName = "Fit";
            bodyTypePath = DatabaseManager.ContentDataSet.Tables["body"].Rows[randomNumber]["Name"].ToString();

            randomNumber = random.Next(1, DatabaseManager.ContentDataSet.Tables["haircolor"].Rows.Count);

            hairstyleColorID = DatabaseManager.ContentDataSet.Tables["haircolor"].Rows[randomNumber]["HexColor"].ToString();
            hairstyleColorName = DatabaseManager.ContentDataSet.Tables["haircolor"].Rows[randomNumber]["Name"].ToString();
            hairstyleColor = DatabaseManager.HexToColor(hairstyleColorID);

            randomNumber = random.Next(1, DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows.Count);

            hairstylePath = DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows[randomNumber]["FileName"].ToString();
            hairstyleName = DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows[randomNumber]["Name"].ToString();

            name = "Ole";

            int randomNum;
            randomNum = random.Next(0, 10);
            
            if (randomNum <= 2)
                age = random.Next(65, 90);
            if (randomNum <= 5)
                age = random.Next(35, 64);
            if (randomNum > 5)
                age = random.Next(16, 34);

            GetSprites();
            GetFetishes();
        }

        private void GetSprites()
        {
            this.Sprite = new Sprite(GameManager.Content.Load<Texture2D>(@"customer"));
        }

        private void GetFetishes()
        {
            fetishes = new List<string>();
            int[] generatednumbers = new int[3];
            int randomNumber = random.Next(1, 5);

            fetishes.Add(NewFetish(randomNumber));

            generatednumbers[0] = randomNumber;

            if (random.Next(1, 10) > 6)
            {
                while(true)
                {
                    randomNumber = random.Next(1, 5);

                    if (!generatednumbers.Contains(randomNumber))
                    {
                        generatednumbers[1] = randomNumber;
                        break;
                    }
                }
                fetishes.Add(NewFetish(randomNumber));
            }

            if (random.Next(1, 10) > 8)
            {
                while (true)
                {
                    randomNumber = random.Next(1, 5);

                    if (!generatednumbers.Contains(randomNumber))
                    {
                        generatednumbers[2] = randomNumber;
                        break;
                    }
                }
                fetishes.Add(NewFetish(randomNumber));
            }

        }

        private string NewFetish(int i)
        {
            string fetish = "";

            switch (i)
            {
                case 1:
                    fetish = DatabaseManager.ContentDataSet.Tables["skincolor"].Rows[random.Next(0, DatabaseManager.ContentDataSet.Tables["skincolor"].Rows.Count)]["Name"].ToString();
                    break;
                case 2:
                    fetish = DatabaseManager.ContentDataSet.Tables["body"].Rows[random.Next(0, DatabaseManager.ContentDataSet.Tables["body"].Rows.Count)]["Name"].ToString();
                    break;
                case 3:
                    fetish = DatabaseManager.ContentDataSet.Tables["haircolor"].Rows[random.Next(0, DatabaseManager.ContentDataSet.Tables["haircolor"].Rows.Count)]["Name"].ToString();
                    break;
                case 4:
                    fetish = DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows[random.Next(0, DatabaseManager.ContentDataSet.Tables["hairstyle"].Rows.Count)]["Name"].ToString();
                    break;
                case 5:
                    fetish = random.Next(16, 80).ToString();
                    break;
                default:
                    fetish = "curvy";
                    break;
            }
            return fetish;
         }
        

        private string GatherInfo()
        {
            string info;
            info = name + " age "+ age + " is " + bodyTypeName + "\n";
            info += "and his interests is \n";

            foreach (string fetish in fetishes)
            {
                info += fetish + ", ";
            }

            return info;
        }

        public override void Update(GameTime time)
        {
            switch (currentState)
            {
                case State.Entering:
                    this.position.X += 30 * (float)time.ElapsedGameTime.TotalSeconds;

                    bool stop = false;

                    foreach (Customer custommer in GameManager.Objects.OfType<Customer>())
                    {
                        if (custommer == this || custommer.currentState != State.Waiting)
                        {
                            continue;
                        }

                        if (this.CollisionRect.Intersects(custommer.CollisionRect))
                        {
                            stop = true;
                            break;
                        }
                    }

                    if (Position.X > 235) // TODO FIX (wrong way to do this)
                    {
                        stop = true;
                    }

                    if (stop)
                    {
                        this.currentState = State.Waiting;
                    }
                    break;
                case State.Waiting:
                    if (Position.X < 230) // TODO FIX (wrong way to do this)
                    {
                        foreach (Customer custommer in GameManager.Objects.OfType<Customer>())
                        {
                            if (custommer.position.X < this.position.X)
                            {
                                continue;
                            }

                            if (custommer.currentState == State.Waiting)
                            {
                                if (this.CollisionRect.Intersects(custommer.CollisionRect))
                                {
                                    this.currentState = State.Waiting;
                                    break;
                                }
                            }

                            this.currentState = State.Entering;
                        }
                    }

                    MouseState mouse = Mouse.GetState();

                    if (GameManager.PreviousMouseState.LeftButton == ButtonState.Released)
                    {
                        if (mouse.LeftButton == ButtonState.Pressed)
                        {
                            Point mousePoint = new Point(mouse.X, mouse.Y);

                            if (this.CollisionRect.Contains(mousePoint))
                            {
                                if (GameManager.SelectedCustomer == this)
                                {
                                    GameManager.SelectedCustomer = null;
                                    GUIInfoElement.Instance.Remove();
                                    return;
                                }

                                GUIInfoElement.Instance.screenText = GatherInfo();
                                GUIInfoElement.Instance.buttonOneText = "Send away";
                                GUIInfoElement.Instance.ButtonOne = this.Remove();
                                GameManager.SelectedCustomer = this;
                            }
                        }
                    }
                    break;
                case State.Active:
                    break;
                case State.Leaving:
                    if (position.X < 750)
                    {
                        position.X = 750;
                    }

                    this.position.X += 20 * (float)time.ElapsedGameTime.TotalSeconds;

                    if (position.X > 1024)
                    {
                        this.Dispose();
                    }
                    break;
            }
        }

        private Action Remove()
        {
            return this.Dispose;
        }

        public override void Draw(GameTime time, SpriteBatch sb)
        {
            if (this.currentState == State.Active)
                return;
            
            sb.Draw(Sprite.Texture, Position, Sprite.SrcRectangle, skincolor, 0, Sprite.Origin, 1, Sprite.Effect, -0.1f);

#if DEBUG
            sb.Draw(Asset.Image["Pixel"], CollisionRect, new Rectangle(0, 0, 1, 1), new Color(Color.Aquamarine, 128), 0, Vector2.Zero, SpriteEffects.None, 1);
#endif
            if (this == GameManager.SelectedCustomer)
            {
                sb.DrawString(Asset.Font["TextSmall"], "Customer Selected!", new Vector2(10, 700), this.skincolor, 0, Vector2.Zero, 1, SpriteEffects.None, 0.5f);
            }
        }

        public void GetProstituteInfo(List<string> prostituteInfo)
        {
            this.prostituteInfo = prostituteInfo;

            MatchInterestsWithProstituteInfo();
        }

        public void MatchInterestsWithProstituteInfo()
        {
              foreach (string fetish in fetishes)
              {
                   foreach (string info in prostituteInfo)
                   {
                       if (fetish.Equals(info))
                           rating += 2 / fetishes.Count;
                   }
              }
        }

        /// <summary>
        /// Prostitute collects payment and Customer leaves a rating
        /// </summary>
        /// <returns></returns>
        public int CollectPayment()
        {
            Brothel.Instance.Rate((int)rating);
            return payment;
        }

        public override void Dispose()
        {
            if (GameManager.SelectedCustomer == this)
            {
                GameManager.SelectedCustomer = null;
            }

            base.Dispose();
        }
    }
}
