﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Brothelhood
{
    /// <summary>
    /// Static class for preloading assets.
    /// </summary>
    public static class Asset
    {
        private static Dictionary<string, SpriteFont> font = new Dictionary<string, SpriteFont>();
        private static Dictionary<string, SoundEffect> sound = new Dictionary<string, SoundEffect>();
        private static Dictionary<string, Texture2D> image = new Dictionary<string, Texture2D>();

        /// <summary>
        /// Gets a preloaded font.
        /// </summary>
        public static Dictionary<string, SpriteFont> Font
        {
            get { return Asset.font; }
        }

        /// <summary>
        /// Gets a preloaded sound.
        /// </summary>
        public static Dictionary<string, SoundEffect> Sound
        {
            get { return Asset.sound; }
        }

        /// <summary>
        /// Gets a preloaded image.
        /// </summary>
        public static Dictionary<string, Texture2D> Image
        {
            get { return Asset.image; }
        }

        /// <summary>
        /// Preloads assets.
        /// </summary>
        public static void LoadAssets()
        {
            // Fonts
            font.Add("TitleSmall",
                GameManager.Content.Load<SpriteFont>(@"Fonts\Monoton-48"));
            font.Add("TitleLarge",
                GameManager.Content.Load<SpriteFont>(@"Fonts\Monoton-60"));
            font.Add("TextTiny",
                GameManager.Content.Load<SpriteFont>(@"Fonts\PatrickHandSC-14"));
            font.Add("TextSmall",
                GameManager.Content.Load<SpriteFont>(@"Fonts\PatrickHandSC-26"));
            font.Add("TextLarge",
                GameManager.Content.Load<SpriteFont>(@"Fonts\PatrickHandSC-30"));
            font.Add("TextAltSmall",
                GameManager.Content.Load<SpriteFont>(@"Fonts\TheGirlNextDoor-26"));
            font.Add("TextAltLarge",
                GameManager.Content.Load<SpriteFont>(@"Fonts\TheGirlNextDoor-30"));

            // Sounds
            //sound.Add("...", GameManager.Content.Load<SoundEffect>("..."));

            // Images
            //image.Add("...", GameManager.Content.Load<Texture2D>("..."));
            image.Add("Pixel", GameManager.Content.Load<Texture2D>(@"pixel"));
        }
    }
}
