﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace Brothelhood
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D background;

        // Cheats
        private Rectangle rectCheat1;

        public Game1() : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            GameManager.GameSize = new Size(
                this.graphics.PreferredBackBufferWidth = 1024,
                this.graphics.PreferredBackBufferHeight = 768);

#if !DEBUG
            graphics.ToggleFullScreen();
#endif

            DatabaseManager.LoadContent();
            DatabaseManager.LoadBrothel();
            DatabaseManager.DisconnectAll();

            background = this.Content.Load<Texture2D>("background");

            GameManager.CurrentState = GameManager.State.Playing;
            this.rectCheat1 = new Rectangle(920, 4, 100, 20);

            base.Initialize();
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            GameManager.Content = this.Content;
            Asset.LoadAssets();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }
            
            if (GameManager.PreviousMouseState.LeftButton == ButtonState.Released)
            {
                MouseState state = Mouse.GetState();

                if (state.LeftButton == ButtonState.Pressed)
                {
                    Point mousePoint = new Point(state.X, state.Y);

                    if (this.rectCheat1.Contains(mousePoint))
                    {
                        List<int> poscombs = new List<int>() { 1, 2, 3, 4 };
                        foreach (GameObject obj in GameManager.Objects)
                        {
                            if (obj is Prostitute)
                            {
                                Prostitute prost = (Prostitute)obj;

                                if (poscombs.Contains(prost.ID))
                                {
                                    poscombs.Remove(prost.ID);
                                }
                            }
                        }

                        if (poscombs.Count != 0)
                        {
                            new Prostitute(poscombs[0]);
                        }
                    }
                }
            }

            // Update GameManager
            GameManager.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.NonPremultiplied);

            // Draws all GameObjects
            switch (GameManager.CurrentState)
            {
                case GameManager.State.Playing:
                    foreach (GameObject go in GameManager.Objects.ToArray())
                    {
                        go.Draw(gameTime, spriteBatch);
                    }

                    break;
                case GameManager.State.Menu:
                    foreach (GameObject go in GameManager.GuiElements.ToArray())
                    {
                        go.Draw(gameTime, spriteBatch);
                    }

                    break;
            }

            spriteBatch.DrawString(Asset.Font["TextSmall"], "Cash: " + Brothel.Instance.cash.ToString() + "$", Vector2.Zero, Color.Green);

            spriteBatch.DrawString(Asset.Font["TextSmall"], "Rating: " + Brothel.Instance.Rating, new Vector2(0, 80), Color.Yellow);

            spriteBatch.DrawString(Asset.Font["TextSmall"], "Mafia payment: " + Brothel.Instance.Rent + "$", new Vector2(0, 40), Color.LawnGreen);

            spriteBatch.Draw(Asset.Image["Pixel"], new Rectangle(0, 590, 1024, 178), new Rectangle(0, 0, 1, 1), new Color(74, 74, 74), 0, Vector2.Zero, SpriteEffects.None, 0.1f);

            spriteBatch.Draw(background, 
                new Rectangle(0, 0, GameManager.GameSize.Width, GameManager.GameSize.Height), 
                new Rectangle(0, 0, GameManager.GameSize.Width, GameManager.GameSize.Height),
                Color.White, 0, Vector2.Zero, SpriteEffects.None, -1);
            spriteBatch.End();

            //Cheats
            spriteBatch.Begin();
            spriteBatch.Draw(Asset.Image["Pixel"], rectCheat1, Color.DarkBlue);
            spriteBatch.DrawString(Asset.Font["TextTiny"], @"+randomProst.", new Vector2(920, 2), Color.CadetBlue);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
