﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Brothelhood
{
    public static class GameManager
    {
        private static List<GameObject> objects = new List<GameObject>();
        private static List<GameObject> guiElements = new List<GameObject>();
        private static Random random = new Random();
        private static Size gameSize;
        private static ContentManager content;
        private static State currentState;
        private static MouseState mouse;
        private static Customer selectedCustomer;
        private static double customerFrequency;
        private const double FrequencyLimit = 6;

        public enum State
        {
            Playing,
            Menu
        }

        public static List<GameObject> Objects
        {
            get { return GameManager.objects; }
            set { GameManager.objects = value; }
        }
        public static List<GameObject> GuiElements
        {
            get { return GameManager.guiElements; }
            set { GameManager.guiElements = value; }
        }
        public static ContentManager Content
        {
            get { return GameManager.content; }
            set { GameManager.content = value; }
        }
        public static Size GameSize
        {
            get { return GameManager.gameSize; }
            set { GameManager.gameSize = value; }
        }
        public static State CurrentState
        {
            get { return GameManager.currentState; }
            set { GameManager.currentState = value; }
        }
        public static MouseState PreviousMouseState
        {
            get { return mouse; }


        }
        public static Customer SelectedCustomer
        {
            get { return GameManager.selectedCustomer; }
            set { GameManager.selectedCustomer = value; }
        }

        
        /// <summary>
        /// The amount of times the police is going to visit the brothel before stoppping
        /// </summary>
        public static int policeCount = 0;

        public static void Update(GameTime gameTime)
        {
            if (currentState == State.Playing)
            {
                foreach (GameObject go in GameManager.Objects.ToArray())
                {
                    go.Update(gameTime);
                }
            }

            if (currentState == State.Menu)
            {
                foreach (GameObject go in guiElements.ToArray())
                {
                    go.Update(gameTime);
                }
            }


            // Spawns a custommer at random, if sufficient time has passed.
            if (customerFrequency > FrequencyLimit)
            {
                {
                    int limit = 0;
                    foreach (GameObject c in objects)
                    {
                        if (c is Customer)
                        {
                            limit++;
                        }
                    }

                    if (limit < 6)
                    {
                        new Customer();
                    }
                }
                
                customerFrequency = 0;
            }
            else
            {
                customerFrequency += 2 
                    * gameTime.ElapsedGameTime.TotalSeconds
                    * random.NextDouble()
                    * random.NextDouble()
                    + Brothel.Instance.rating * 0.01;
            }

            mouse = Mouse.GetState();
        }
    }
}
