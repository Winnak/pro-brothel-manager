﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace Brothelhood
{
    public class GUIWindow : GameObject
    {
        private Rectangle background;

        private Rectangle rectOne;
        private Rectangle rectTwo;
        private Rectangle rectThree;

        private Action ButtonOne;
        private Action ButtonTwo;
        private Action ButtonThree;

        private string falvourText;
        private string text1;
        private string text2;
        private string text3;
        
        private SpriteFont font = Asset.Font["TextSmall"];
        private Texture2D windowBackround;

        public GUIWindow(string flavourText, Action action, string buttonString)
        {
            this.falvourText = flavourText;
            this.background = new Rectangle(254, 251, 512, 384);
            this.windowBackround = GameManager.Content.Load<Texture2D>(@"pixel.png");

            this.text1       = buttonString;

            this.ButtonOne   = action;

            this.rectOne     = new Rectangle(260, 564, 500, 64);

            GameManager.Objects.Add(this);
            GameManager.GuiElements.Add(this);
            GameManager.CurrentState = GameManager.State.Menu;
        }

        public GUIWindow(
            string flavourText, 
            Action action, string button1String,
            Action action2, string button2String)
        {
            this.falvourText = flavourText;
            this.background = new Rectangle(254, 251, 512, 384);
            this.windowBackround = GameManager.Content.Load<Texture2D>(@"pixel.png");

            this.text1       = button1String;
            this.text2       = button2String;

            this.ButtonOne   = action;
            this.ButtonTwo   = action2;

            this.rectOne     = new Rectangle(260, 564, 247, 64);
            this.rectTwo     = new Rectangle(513, 564, 247, 64);

            GameManager.Objects.Add(this);
            GameManager.GuiElements.Add(this);
            GameManager.CurrentState = GameManager.State.Menu;
        }

        public GUIWindow(
            string flavourText, 
            Action action, string button1String,
            Action action2, string button2String,
            Action action3, string button3String)
        {
            this.falvourText = flavourText;
            this.background = new Rectangle(254, 251, 512, 384);
            this.windowBackround = GameManager.Content.Load<Texture2D>(@"pixel.png");

            this.text1       = button1String;
            this.text2       = button2String;
            this.text3       = button3String;

            this.ButtonOne   = action;
            this.ButtonTwo   = action2;
            this.ButtonThree = action3;

            this.rectOne     = new Rectangle(260, 564, 165, 64);
            this.rectTwo     = new Rectangle(428, 564, 164, 64);
            this.rectThree   = new Rectangle(595, 564, 165, 64);

            GameManager.Objects.Add(this);
            GameManager.GuiElements.Add(this);
            GameManager.CurrentState = GameManager.State.Menu;
        }

        public override void Draw(GameTime gametime, SpriteBatch sb)
        {
            // Draw background
            sb.Draw(this.windowBackround, this.background, new Rectangle(0, 0, 1, 1), Color.DarkGray, 0, Vector2.Zero, SpriteEffects.None, 0.8f);
            sb.DrawString(this.font, this.falvourText, new Vector2(254, 251), Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            // Draw buttons
            if (this.text3 != null)
            {
                sb.Draw(this.windowBackround, this.rectOne, new Rectangle(0, 0, 1, 1), Color.Gray, 0, Vector2.Zero, SpriteEffects.None, 0.9f);
                sb.Draw(this.windowBackround, this.rectTwo, new Rectangle(0, 0, 1, 1), Color.Gray, 0, Vector2.Zero, SpriteEffects.None, 0.9f);
                sb.Draw(this.windowBackround, this.rectThree, new Rectangle(0, 0, 1, 1), Color.Gray, 0, Vector2.Zero, SpriteEffects.None, 0.9f);

                sb.DrawString(this.font, this.text1, new Vector2(264, 568), Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1);
                sb.DrawString(this.font, this.text2, new Vector2(432, 568), Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1);
                sb.DrawString(this.font, this.text3, new Vector2(599, 568), Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1);

                return;
            }
            
            if (this.text2 != null)
            {
                sb.Draw(this.windowBackround, this.rectOne, new Rectangle(0, 0, 1, 1), Color.Gray, 0, Vector2.Zero, SpriteEffects.None, 0.9f);
                sb.Draw(this.windowBackround, this.rectTwo, new Rectangle(0, 0, 1, 1), Color.Gray, 0, Vector2.Zero, SpriteEffects.None, 0.9f);

                sb.DrawString(this.font, text1, new Vector2(264, 568), Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1f);
                sb.DrawString(this.font, text2, new Vector2(517, 568), Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1f);

                return;
            }

            sb.Draw(this.windowBackround, this.rectOne, new Rectangle(0, 0, 1, 1), Color.Gray, 0, Vector2.Zero, SpriteEffects.None, 0.9f);

            sb.DrawString(this.font, this.text1, new Vector2(264, 568), Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 1);

        }

        public override void Update(GameTime gametime)
        {
            MouseState state = Mouse.GetState();

            if (state.LeftButton == ButtonState.Pressed)
            {
                Point mousePoint = new Point(state.X, state.Y);

                if (this.rectThree != null)
                {
                    if (this.rectThree.Contains(mousePoint))
                    {
                        this.ButtonOne();
                        this.Dispose();
                        return;
                    }
                }

                if (this.rectTwo != null)
                {
                    if (this.rectTwo.Contains(mousePoint))
                    {
                        this.ButtonTwo();
                        this.Dispose();
                        return;
                    }
                }

                if (this.rectOne.Contains(mousePoint))
                {
                    this.ButtonOne();
                    this.Dispose();
                    return;
                }
            }
        }

        public override void Dispose()
        {
            GameManager.CurrentState = GameManager.State.Playing;
            GameManager.GuiElements.Remove(this);
            base.Dispose();
        }
    }
}
