﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Brothelhood
{
    class Police : Customer
    {
        //private int bribeCost;
        private int returnAmount;

        public Police()
        {
            Random rnd = new Random();
            int policeReturn = rnd.Next(1, 5);
            returnAmount = policeReturn;
        }


        public override void Update(Microsoft.Xna.Framework.GameTime time)
        {
            new GUIWindow("YOLO", BribePolice(), "BRIBE", EntertainPolice(), "ENTERTAIN", IgnorePolice(), "IGNORE");
        }

        private Action BribePolice()
        {
            return this.Bribe;
        }

        private void Bribe()
        {
            Brothel.Instance.notoriety = 0;
            //Brothel.Instance.cash -= bribeCost;
            GameManager.Objects.Remove(this);
            GameManager.policeCount = 0;
        }

        private Action EntertainPolice()
        {
            return this.Entertain;
        }

        private void Entertain()
        {
            //selectgirl
            Brothel.Instance.notoriety /= 2;
            GameManager.Objects.Remove(this);
            GameManager.policeCount -= 1;
        }

        private Action IgnorePolice()
        {
            return this.Ignore;
        }

        private void Ignore()
        {
            //go away mr. police
            Random rnd = new Random();
            int chanceOfLeaving = rnd.Next(2);

            if (chanceOfLeaving==0)
            {
                GameManager.policeCount -= 1;
            }
            GameManager.Objects.Remove(this);
           
        }

    }
}
