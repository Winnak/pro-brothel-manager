﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Brothelhood
{
    public class Sprite
    {
        private Texture2D texture;
        private Rectangle srcRectangle;
        private Color color;
        private Vector2 origin;
        private Vector2 scale;
        private int zIndex;
        private SpriteEffects effect;

        /// <summary>
        /// Gets or sets the <see cref="Texture2D"/>.
        /// </summary>
        public Texture2D Texture
        {
            get { return this.texture; }
            set { this.texture = value; }
        }

        /// <summary>
        /// Gets or sets the source <see cref="Rectangle"/>.
        /// </summary>
        public Rectangle SrcRectangle
        {
            get { return this.srcRectangle; }
            set { this.srcRectangle = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Color"/>.
        /// </summary>
        public Color Color
        {
            get { return this.color; }
            set { this.color = value; }
        }

        /// <summary>
        /// Gets or sets the origin of the <see cref="Sprite" /> image.
        /// </summary>
        public Vector2 Origin
        {
            get { return this.origin; }
            set { this.origin = value; }
        }

        /// <summary>
        /// Gets or sets the scale.
        /// </summary>
        public Vector2 Scale
        {
            get { return this.scale; }
            set { this.scale = value; }
        }

        /// <summary>
        /// Gets or sets the Z index.
        /// </summary>
        public int ZIndex
        {
            get { return this.zIndex; }
            set { this.zIndex = value; }
        }

        /// <summary>
        /// Gets or sets the sprite effects.
        /// </summary>
        public SpriteEffects Effect
        {
            get { return this.effect; }
            set { this.effect = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sprite"/> class.
        /// </summary>
        /// <param name="texture">Sprite texture.</param>
        public Sprite(Texture2D texture)
        {
            this.color = Color.White;
            this.origin = Vector2.Zero;
            this.scale = Vector2.One;
            this.zIndex = 0;
            this.effect = SpriteEffects.None;

            this.texture = texture;
            this.srcRectangle = new Rectangle(0, 0, texture.Width, texture.Height);
        }
    }
}
