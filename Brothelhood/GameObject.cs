﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Brothelhood
{
    /// <summary>
    /// A game object is a super class for all classes that requires updating.
    /// </summary>
    public abstract class GameObject : IDisposable
    {
        private Sprite sprite;
        protected Vector2 position;

        /// <summary>
        /// Gets or sets the <see cref="Sprite" />.
        /// </summary>
        protected Sprite Sprite
        {
            get { return sprite; }
            set { sprite = value; }
        }

        /// <summary>
        /// Gets og sets the position of the <see cref="GameObject"/>.
        /// </summary>
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameObject"/> class.
        /// </summary>
        public GameObject() 
        {
            GameManager.Objects.Add(this);
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="GameObject" /> class.
        /// </summary>
        ~GameObject()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Updates the <see cref="GameObject" />
        /// </summary>
        /// <param name="gametime">Current game time.</param>
        public abstract void Update(GameTime time);

        /// <summary>
        /// Draws this instance of the <see cref="GameObject" /> class.
        /// </summary>
        /// <param name="sb">SpriteBatch used to draw textures.</param>
        public abstract void Draw(GameTime time, SpriteBatch sb);

        /// <summary>
        /// Disposes the GameObject.
        /// </summary>
        /// <param name="forced">Did it not happen automaticly?</param>
        public virtual void Dispose()
        {
            this.Dispose(true);
        }

        /// <summary>
        /// Disposes the game object.
        /// </summary>
        private void Dispose(bool forced)
        {
            GameManager.Objects.Remove(this);

            GC.SuppressFinalize(this);
        }
    }
}
