﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Brothelhood
{
    interface ICollidable
    {
        Rectangle CollisionRect { get; }
    }
}
